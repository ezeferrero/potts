This code was originally implemented for:  
"q-state Potts model metastability study using optimized GPU-based Monte Carlo algorithms",  
Ezequiel E. Ferrero, Juan Pablo De Francesco, Nicolás Wolovick, Sergio A. Cannas  
http://arxiv.org/abs/1101.0876  
Computer Physics Communications Volume 183, Issue 8, August 2012, Pages 1578–1587  
http://dx.doi.org/10.1016/j.cpc.2012.02.026  
  
The code is freely available under GNU GPL v3.  
  
Version 1.1:  
- Added the CPU version. Minor code cleanups.  
  
Version 1.0:  
- Original release used in the article.  


